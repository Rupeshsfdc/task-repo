trigger UpdateCPActivenOnOppty on Customer_Project__c (after insert)
{
List<Opportunity> opps=new List<Opportunity>();
for(Customer_Project__c cp:Trigger.New){
if(cp.Status__c=='Active'){
Opportunity opp= new Opportunity(id=cp.Opportunity__c);
opp.Active_Customer_Project__c = True;
opps.add(opp);
}
}
update opps;
}
-------------------------------------------------------------------------------------


@isTest
Public Class UpdateCPActivenOnOpptyTest{
static testMethod void updateCustomerProjectOnopty(){
//Inserting first Opportunity
Opportunity opp1= new Opportunity();
opp1.Name='Test Opportunity 1';
Date myDate = Date.newinstance(2012,6,24);
opp1.CloseDate=myDate;
opp1.StageName='Prospecting';
insert opp1;
//Inserting a Customer Project with Status equal to Active for above opp1
Customer_Project__c cp1=new Customer_Project__c();
cp1.Name='Test 1';
cp1.Status__c='Active';
cp1.Opportunity__c=opp1.id;
insert cp1;
Opportunity o1=[Select Active_Customer_Project__c from Opportunity where id=:opp1.id];
System.assertEquals(true,o1.Active_Customer_Project__c);
//Inserting first Opportunity
Opportunity opp2= new Opportunity();
opp2.Name='Test Opportunity 1';
Date d = Date.newinstance(2012,6,24);
opp2.CloseDate=d;
opp2.StageName='Prospecting';
insert opp2;
//Inserting a Customer Project with Status equal to Inactive for above opp2
Customer_Project__c cp2=new Customer_Project__c();
cp2.Name='Test 2';
cp2.Status__c='Inactive';
cp2.Opportunity__c=opp2.id;
insert cp2;
Opportunity o2=[Select Active_Customer_Project__c from Opportunity where id=:opp2.i
d];
System.assertEquals(false,o2.Active_Customer_Project__c);
}
}
---------
